'use strict';

module.exports.applicationLog = async (err, req, res, next) => {
  try{
    /* 
      TODO:
      * reemplazar por algo más útil como log4j...
      * habilitar servicio de mails    
      * logs específicos para la base de datos
     */
    console.log(err);
    next(err)
  } catch(error){
    next(error);
  }
};

module.exports.clientErrorHandler = async (err, req, res, next) => {
  try{
    if (err.name && err.code){
      res.sendStatus(500);
    }
    else{
      /*
       * En realidad va:
         next(err);
         
         para que la condición se excluyente, y caiga por omisión en:
         defaultErrorHandler
       */
      res.sendStatus(err.statusCode || 400);
    }
  } catch(error){
    next(error);
  }
};

module.exports.defaultErrorHandler = (err, req, res, next) => {
  res.sendStatus(500);
};