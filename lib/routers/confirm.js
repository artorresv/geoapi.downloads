'use strict';

const fs = require('fs'),
      path = require('path'),
      util = require('../util.js'),
      nconf = require('nconf'),
      {fork} = require('child_process'),
      express = require('express'),
      talisman = require('talismanjs');

module.exports = () => {
  const router = express.Router();
  
  router.get('/getfeature/file/confirm/', async (req, res, next) => {
    try {
      const qParams = await util.validateQueryParams(req.query);
      
      if (!qParams.valid){
        throw {statusCode: 400};
      }
      
      const confirmationMailTpl = await talisman.create('./views/downloadMail.tpl'),
            filePath = path.join(nconf.get('uploadDir'), req.query.reqId);
      
      fs.access(filePath, fs.constants.R_OK | fs.constants.W_OK, async (err) => {
        if (err){
          res.status(404).send();
        }
        else{
          const formatedDate = await util.getLocaleDateTime(new Date()),
                query = await fork('./lib/subprocess/async-query.js', [],
                  {
                    env: {'NODE_ENV': (req.app.locals.isDev?'development':'production')},
                    stdio: ['pipe', fs.openSync('./logs/subprocess.out', 'a'), fs.openSync('./logs/subprocess.err', 'a'), 'ipc']
                  }
                );
                    
          query.on('error', (err) => {
            console.log('Failed to start subprocess: \n' + err);
            res.status(500).send();
          });
                    
          query.send({
            file: {path: filePath, filename: req.query.reqId},
            email: req.query.email,
            layerId: req.query.layer
          });
          
          query.disconnect();
          
          res.status(202).send(await confirmationMailTpl.set({
              downloadLink: '',
              subject: `<strong style="color:red">Gracias su confirmaci&oacute;n</strong>, en breve recibir&aacute; un correo electr&oacute;nico con el enlace para descargar sus datos`,
              linkRel: '',
              layerID: req.query.layer,
              contact1: '',
              contact2: util.cfg.app.emailOptions.cc,
              userName: '',
              userMail: req.query.email,
              observation1: '',
              queryDate: `${formatedDate.date} a las ${formatedDate.time}`,
              expirationDate: ''
            }).toString());
        }
      });
    }
    catch (err) {
      next(err);
    }
  });
  
  return router;
};