'use strict';

const util = require('../util.js'),
      nconf = require('nconf'),
      multer = require('multer'),
      upload = multer(util.cfg.app.ogr.upload),
      ogr2ogr = require('ogr2ogr'),
      express = require('express'),
      shortid = require('shortid'),
      talisman = require('talismanjs');

module.exports = () => {
  const router = express.Router();  
  
  router.post('/getfeature/file/upload/', upload.single('geometry'), async (req, res, next) => {
    try {
      const confirmationMailTpl = await talisman.create('./views/downloadMail.tpl'),
            locDate = new Date(),
            formatedDate = await util.getLocaleDateTime(locDate);
      
      await req.app.locals.transporter.sendMail({
        from: `"${util.cfg.app.creator}" ${nconf.get('emailService').auth.user}`,
        to: req.body.email,
        cc: req.app.locals.isDev?'':util.cfg.app.emailOptions.cc,
        subject: util.cfg.app.emailOptions.subjectOnConfirmation,
        html: await confirmationMailTpl.set({
          downloadLink: util.cfg.app.url.base + util.cfg.app.url.confirmation + `?email=${req.body.email}&layer=${req.body.layer}&reqId=${req.file.filename}`,
          subject: `<strong style="color:red">Para iniciar su consulta de click en el siguiente enlace:</strong>`,
          linkRel: ' para confirmar',
          layerID: req.body.layer,
          contact1: '',
          contact2: util.cfg.app.emailOptions.cc,
          userName: '',
          userMail: req.body.email,
          observation1: '<li>El enlace para confirmar su consulta estar&aacute; disponible <strong>&uacute;nicamente durante las pr&oacute;ximas 72 horas</strong>.</li>',
          queryDate: `${formatedDate.date} a las ${formatedDate.time}`,
          expirationDate: `${await util.getLocaleDateTime(locDate, {offsetDays: 3}).date} a las ${formatedDate.time}`
        }).toString()
      });
       
      res.status(201).send();
    }
    catch (err) {
      next(err);
    }
  });
    
  return router;
};