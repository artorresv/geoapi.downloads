'use strict';

const db = require('../db.js'),
      fs = require('fs'),
      rp = require('request-promise'),
      util = require('../util.js'),
      path = require('path'),
      nconf = require('nconf'),
      ogr2ogr = require('ogr2ogr'),
      shortid = require('shortid'),
      archiver = require('archiver'),
      talisman = require('talismanjs'),
      stringify = require('csv-stringify'),
      nodeMailer = require('nodemailer'),
      QueryStream = require('pg-query-stream');

let NODE_ENV, isDev, transporter;

const mailInfo = {layerId: '', to:'', reqId:'', from: '', cc: ''};

const exceptionMail = async () => {  
  try {
    const qDate = await util.getLocaleDateTime(new Date());
    const downloadMailTpl = await talisman.create('./views/downloadMail.tpl');
    
    await transporter.sendMail({
      from: mailInfo.from,
      to: mailInfo.to,
      replyTo: mailInfo.cc,
      cc: mailInfo.cc,
      subject: util.cfg.app.emailOptions.subjectOnException,
      html: await downloadMailTpl.set({
        downloadLink: '',
        subject: '<strong style="color:red">Ocurri&oacute; un error al procesar su solicitud</strong>. Responda directamente a este correo para reportarnos el incidente (clave de solicitud: <strong>' + mailInfo.reqId + '</strong>)',
        linkRel: '',
        layerID: mailInfo.layerId,
        contact1: '',
        contact2: mailInfo.cc,
        userName: '',
        userMail: mailInfo.to,
        observation1: '',
        queryDate: `${qDate.date} a las ${qDate.time}`,
        expirationDate: ''
      }).toString()
    });
  }
  catch (err) {
    console.log(err);
  }
};

const getData = async ({file, email, layerId}) => {
  let client = null;
  const staticDir = nconf.get('staticDir');
  try {
    const geojson = await ogr2ogr(file.path).options(util.cfg.app.ogr.transform.cfg).promise();
      
      // Validación general del GeoJSON
      if (!geojson.features || geojson.features.length !== 1 || geojson.features[0].type !== 'Feature'){
        /* 
         * En general la conversión únicamente genera geometrías validas, en otros casos devuelve un error y nunca
         * se llegaría a este punto.
         * No existe el objeto features o no tiene elementos: La capa no tenía geometrías válidas
         * Tiene más de un elemento "features": Puede ocurrir, si el usuario subió una capa con multipolígonos,
         * al hacer la conversión a JSON, se fuerza la generación de polígonos simples, de modo que los multipolígonos
         * se podrían (pero depende del tipo de dataset del usuario) dividir en varios polígonos individuales
         */
        throw new Error('No existe el objeto features, no tiene elementos o tiene más de uno');
      }
      
      // Validación de la geometría
      if (geojson.features[0].geometry.type !== 'Polygon') {
        throw new Error('La geometría no es de tipo polygon');
      }
      
      // Validación espacial del GeoJSON
      /* 
       * Ninguna por el momento, es decir, no limita el área, el número de vértices ni otras cosas
       */
      
      if (geojson.features.properties){
        delete geojson.features.properties;
      }
                  
      const sqlParams = {headers: {}, fields: '', db: 'geodb', cluster: false},
            md = (await rp({url: util.cfg.app.url.base + util.cfg.app.url.metadata + layerId + '.json', gzip: true, json:true}));
      
      if (md.keywords && md.keywords.cluster === 'true'){
        layerId = util.cfg.app.displayNames[layerId];
        sqlParams.db = 'snibdb';
        sqlParams.cluster = true;
      }
            
      client = await db.database[sqlParams.db].connect();
      
      const tblHeaders = (await client.query(`
        SELECT string_agg(${sqlParams.cluster ? "concat('\"', column_name, '\"')" : "column_name"}, ',')
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE table_name = '${layerId}' AND
        column_name NOT IN (${util.cfg.app.excludedFields})
      `)).rows[0].string_agg;
            
      sqlParams.fields = tblHeaders;
      
      if (sqlParams.cluster){
        sqlParams.fields = sqlParams.fields.replace('"ultimafechaactualizacion"', 'to_char(ultimafechaactualizacion, \'YYYY-MM-DD\') as ultimafechaactualizacion');
        sqlParams.headers = tblHeaders.replace(/\"/g,'').split(',');
      }
      else{
        sqlParams.fields += ',ST_AsText(the_geom) AS geometria';
        sqlParams.headers = tblHeaders.split(',');
        sqlParams.headers.push('geometria');
      }
      
      const dateCmp = util.cfg.re.date.exec(md.ISOLastModified || md.lastModified).splice(1),
            locDate = new Date(),
            formatedDate = await util.getLocaleDateTime(locDate),
            formatedTime = formatedDate.time,
            maxZipEntries = 2,
            downloadMailTpl = await talisman.create('./views/downloadMail.tpl'),
            uuid = shortid.generate(),
            fNames = {
                       zip: md.id + '.' + dateCmp.join('.') + '.' + uuid + '.csv.zip',
                       csv: md.id + '.csv',
                       // pdf: md.id + '.pdf',
                       kml: 'limites.kml',
                       txt: 'importante.txt'
                     },
            zip = fs.createWriteStream(staticDir + fNames.zip).on('error', err => {throw err}),
            archive = archiver('zip', {zlib: {level: 9}}).on('error', err => {throw err}).on('warning', err => {throw err});
                                   
      zip.on('close', async () => {
        await transporter.sendMail({
          from: mailInfo.from,
          to: mailInfo.to,
          replyTo: isDev?'':md.dataContact.email,
          cc: mailInfo.cc,
          subject: util.cfg.app.emailOptions.subject,
          html: await downloadMailTpl.set({
            downloadLink: util.cfg.app.url.base + util.cfg.app.url.downloads + fNames.zip,
            subject: `Registros extraídos del mapa "${md.title}", ubicados en la delimitación geográfica proporcionada por el usuario`,
            linkRel: ' para descarga',
            layerID: md.id,
            contact1: md.dataContact.email,
            contact2: util.cfg.app.emailOptions.cc,
            userName: '',
            userMail: email,
            observation1: '<li>Los datos estarán disponibles para descarga <strong>únicamente durante las próximas 72 horas</strong>.</li>',
            queryDate: `${formatedDate.date} a las ${formatedDate.time}`,
            expirationDate: `${await util.getLocaleDateTime(locDate, {offsetDays: 3}).date} a las ${formatedDate.time}`
          }).toString()
        });
        
        // Borra el archivo que subió el usuario
        fs.unlink(file.path, err => {if (err) {throw err};});
        
        // console.log('La consulta se realizó exitosamente y se envío la notificación al usuario');
      });
      
      archive.on('entry', fileInfo => {
        if (fileInfo.type === 'file'){
          fs.unlink(fileInfo.sourcePath, err => {if (err) {throw err};});
        }
        
        if (archive._entriesProcessedCount === maxZipEntries){
          archive.finalize();
        }
      }).pipe(zip);
      
      const intersectGeom = JSON.stringify(geojson.features[0].geometry),
            query = new QueryStream(`
              SELECT ${sqlParams.fields}
              FROM public.${layerId}
              WHERE ST_Intersects(the_geom, ST_SetSRID(ST_GeomFromGeoJSON('${intersectGeom}'), 4326))
            `, [], {batchSize:250}),
            txtTpl = await talisman.create('./views/importante.tpl'),
            kmlTpl = await talisman.create('./views/kml.tpl'),
            htmlPermLink = `${util.cfg.app.url.base}metadatos/doc/html/${md.id}.html`,
            dataPermLink = `${util.cfg.app.url.base}servicios/datos/descarga?l=${md.id}&f=${file.filename}&t=${dateCmp[2] + dateCmp[1] + dateCmp[0]}`,
            // webPermLink = `${util.cfg.app.url.base}#!l=${md.id}:1@f=${file.filename}@t=${dateCmp[2] + dateCmp[1] + dateCmp[0]}`,
            webPermLink = 'No disponible',
            mdSubject = 'ubicados en la delimitación geográfica proporcionada por el usuario.',
            queryMD = (await client.query(`
              SELECT COUNT(*) AS count, ST_AsKML(g.geom) AS kml, ST_AsGeoJSON(ST_Envelope(g.geom), 15, 1) AS bbox
              FROM ${layerId}, (
                SELECT ST_SetSRID(ST_GeomFromGeoJSON('${intersectGeom}'),4326) AS geom
              ) AS g
              WHERE ST_Intersects(the_geom, g.geom)
              GROUP BY g.geom
            `)).rows[0];
            
            /* 
             * Por ahora omito metadato en pdf...
             */
            /* const queryBbox = JSON.parse(queryMD.bbox).bbox; */
                        
      await client.query(query)
            .on('error', err => {throw err})
            .pipe(stringify({quoted:true, columns:sqlParams.headers, header: true}))
            .pipe(fs.createWriteStream(staticDir + uuid + '.' + fNames.csv))
            .on('error', err => {throw err})
            .on('close',() => archive.file((staticDir + uuid + '.' + fNames.csv), {name: fNames.csv}));

      await kmlTpl.set({geom: queryMD.kml, mdTitle: md.title, layerid: md.id, webLink: webPermLink, cYear: locDate.getFullYear(), locationDesc: 'Registros ubicados en la delimitación geográfica proporcionada por el usuario.'}).toStream()
            .on('error', err => {throw err})
            .pipe(fs.createWriteStream(staticDir + uuid + '.' + fNames.kml))
            .on('error', err => {throw err})
            .on('close', () => archive.file((staticDir + uuid + '.' + fNames.kml), {name: fNames.kml}));
      
      await txtTpl.set({mdCreator: util.cfg.app.creator, htmlLink: htmlPermLink, dataLink: dataPermLink, webLink: webPermLink, recordCount: queryMD.count, suuid: uuid, mdTitle: md.title, shortLastMod: dateCmp.join('.'), padLayerid: md.id.padEnd(13), layerid: md.id, locDate: formatedDate.date, locTime: formatedDate.time, mdTitleDesc: mdSubject, locLastMod: dateCmp[2] + ' de ' + util.cfg.app.months[dateCmp[1]] + ' de ' + dateCmp[0]}).toStream()
            .on('error', err => {throw err})
            .pipe(fs.createWriteStream(staticDir + uuid + '.' + fNames.txt))
            .on('error', err => {throw err})
            .on('close',() => archive.file((staticDir + uuid + '.' + fNames.txt), {name: fNames.txt}));
  }
  catch (err) {
    console.log(err);
    await exceptionMail();
  }
  finally {
    if (client !== null) {
      client.release();
    }
  }
};
 
process.on('message', async ({file, email, layerId}) => {  nconf.argv().env('__').defaults({'NODE_ENV': 'development'});

  nconf
    .defaults({'conf': path.join(__dirname, '../../', `${process.env.NODE_ENV}.config.json`)})
    .file(nconf.get('conf'));
    
  NODE_ENV = nconf.get('NODE_ENV');
  isDev = NODE_ENV === 'development';
    
  transporter = await nodeMailer.createTransport(nconf.get('emailService'));
    
  require('../db.js').initialize(nconf.get('pg'));

  mailInfo.layerId = layerId;
  mailInfo.to = email;
  mailInfo.reqId = file.filename;
  mailInfo.from = `"${util.cfg.app.creator}" ${nconf.get('emailService').auth.user}`;
  mailInfo.cc = (isDev?'':util.cfg.app.emailOptions.cc);
    
  try {    
    await getData({file, email, layerId});
  }
  catch (err) {
    console.log(err);
    exceptionMail();
  }
});

process.on('exit', (code) => {
  if (isDev){
    console.log(`El subproceso terminó (código: ${code})`);    
  }
});

process.on('uncaughtException', (err) => {
  console.log(err);
  exceptionMail();
});

process.on('unhandledRejection', (reason, p) => {
  console.log('Unhandled Rejection at:', p, 'reason:', reason);
  exceptionMail();
});
