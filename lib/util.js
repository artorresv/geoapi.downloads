'use strict';

const shortid = require('shortid'),
      nconf = require('nconf'),
      multer = require('multer');

const cfg = {
  app: {
    ogr: {
      transform: {
        cfg: ['-limit', '1', '-t_srs', 'EPSG:4326', '-nlt', 'POLYGON']
      },
      upload: {
        storage: multer.diskStorage({
          destination: nconf.get('uploadDir'),
          filename: getTemporalFileName
        }),
        fileFilter: uploadFilter,
        limits: {fields: 3, fileSize: 10000000, files: 1, parts: 4}
      }
    },
    creator: 'Geoportal del Sistema Nacional de Información sobre Biodiversidad (SNIB)',
    emailOptions: {
      subject: 'Solicitud de descarga de datos',
      subjectOnException: 'Problema al procesar su solicitud de descarga de datos',
      subjectOnConfirmation: 'Confirmación de solicitud de descarga de datos',
      cc: '"Subcoordinación de Informática" si@conabio.gob.mx'
    },
    publisher: 'Comisión Nacional para el Conocimiento y Uso de la Biodiversidad',
    pdf: {producer: 'PDFMake with PDFKit'},
    months: {'01':'enero','02':'febrero','03':'marzo','04':'abril','05':'mayo','06':'junio','07':'julio','08':'agosto','09':'septiembre','10':'octubre','11':'noviembre','12':'diciembre'},
    url: {
      base: 'http://geoportal.conabio.gob.mx/',
      metadata: 'metadatos/doc/dc.json/',
      confirmation: 'servicios/datos/avisos/',
      downloads: 'descargas/mapas/dinamicos/'
    },
    sharedCacheHeaders: {'Cache-Control': 'public, max-age=3600, s-maxage=15552000'},
    privateCacheHeaders: {'Cache-Control': 'public, max-age=600, must-revalidate'},
    formats: {
      geojson: "{\"type\":\"FeatureCollection\",\"totalFeatures\":null,\"features\":[],\"crs\":{\"type\":\"name\",\"properties\": {\"name\":\"urn:ogc:def:crs:EPSG::3857\"}}}"
    },
    excludedFields: "'area','perimeter','length','the_geom','cov_','cov_id','spid','ispid','geoid'",
    displayFields: "\"grupobio\",\"subgrupobio\",\"familiavalida\",\"generovalido\",\"especievalida\",\"nom059\",\"cites\",\"iucn\",\"prioritaria\",\"exoticainvasora\",\"longitud\",\"latitud\",\"estadomapa\",\"municipiomapa\",\"localidad\",\"fechacolecta\",\"anp\",\"probablelocnodecampo\",\"categoriaresidenciaaves\",\"formadecrecimiento\",\"fuente\",\"taxonextinto\",\"usvserieVI\",\"urlejemplar\",\"idejemplar\",\"ultimafechaactualizacion\"",
    displayNames: {
      bacterias: "snibbactgw",
      invertebrados: "snibinvegw",
      peces: "snibpecegw",
      protoctistas: "snibprotgw",
      reptiles: "snibreptgw",
      plantas: "snibplangw",
      aves: "snibavesgw",
      anfibios: "snibanfigw",
      mamiferos: "snibmamigw",
      hongos: "snibhonggw"
    }
  },
  re: {
    date: /^(\d{4})-(\d{2})-(\d{2})$/,
    layerId: /\w+/i,
    requestId: /[\w-_\.]+/i,
    email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i
  }
};

function getTemporalFileName(req, file, cb) {
  let fName = shortid.generate();
  
  if (file.originalname.endsWith('zip') || file.originalname.endsWith('kmz')){
    fName += '.zip';
  }
  
  cb(null, fName);
}

function uploadFilter(req, file, cb) {
  
  /* 
  NOTAS:
  * Validar aquí el dominio de los campos de texto, etc.
  * Validar aquí aspectos relacionados con el contenido del archivo
    (e.g. número de archivos en el shapefile, etc.)
   */
  
  if (!file){
    cb(new Error('El archivo con los datos es requerido'));
  }
  
  if (!req.body.layer || !req.body.email){
    cb(new Error('Los campos "layer" y "email" son requeridos'));
  }
  
  cb(null, true);
}

module.exports.cfg = cfg;

module.exports.getLocaleDateTime = (isoDate, opt = {lang: 'es', offsetDays: 0}) => {
  
  if (opt.offsetDays){
    isoDate.setDate(isoDate.getDate() + opt.offsetDays);
  }
  
  return {
    date: isoDate.getDate() + ' de ' + cfg.app.months[(isoDate.getMonth() + 1).toString().padStart(2, '0')] + ' de ' + isoDate.getFullYear(),
    time: isoDate.getHours().toString().padStart(2, '0') + ':' + isoDate.getMinutes().toString().padStart(2, '0') + ':' + isoDate.getSeconds().toString().padStart(2, '0') + ' horas (UTC-' + isoDate.getTimezoneOffset()/60 + ')'
  };
};

module.exports.validateQueryParams = (params) => {
  const result = {valid: true};
  
  if (!params.email || !params.layer || !params.reqId){
    result.valid = false;
  }
  
  if (!cfg.re.email.test(params.email) || !cfg.re.layerId.test(params.layer) || !cfg.re.requestId.test(params.reqId)){
    result.valid = false;
  }
      
  return result;
};
