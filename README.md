**REQUERIMIENTOS**

  * Ubuntu > 18.x
  * Nodejs > 8.x
  * npm > 6.x (creo que se instala por omisión con nodejs)               * [pm2](https://pm2.io) > 3.3.x

**INSTALACION**

0. Asume que los siguientes directorios existen y el usuario que ejecuta este script tiene permisos para escribir en ellos:

    `/datos/geoapi/upload/`
    
    `/datos/geoapi/temp/`

1. Asumiendo que este directorio existe y se tienen permisos de escritura:    

    `cd /opt/webserver`

2. Clonar el repositorio:

    `git clone https://artorresv@bitbucket.org/artorresv/geoapi.downloads.git`
    
3. El procedimiento anterior crea un directorio con el código fuente:

    `cd geoapi.downloads`

4. Instalar las dependencias de nodejs:

    `NODE_ENV=production npm install`

5. Después de instaladas, copiar los siguientes archivos (revisar el contenido para ver qué puerto usa, etc.) según la instalación sea para desarrollo o para producción:

    Producción: `scp geos1.conabio.gob.mx:/home/atorres/bin/geoportal/servicios/geoapi.downloads/production.config.json .`
    
    Desarrollo: `scp geos1.conabio.gob.mx:/home/atorres/bin/geoportal/servicios/geoapi.downloads/development.config.json .`

**USO**

1. Incio normal de la aplicación:
    
    a. Con npm:
    
       `NODE_ENV=production npm run start`
       
       
    b. Con pm2 (ver ecosystem.config.json para más detalles):
    
    
       `pm2 start ecosystem.config.json --env production`
      
      Los comandos disponibles para pm2 se pueden ver con `pm2 --help`, algunos Ãºtiles son:
      
       `pm2 show GeoAPI`: para ver todos los detalles de cada instancia de GeoAPI
      
       `pm2 ls`: para ver la lista completa de procesos activos
       
2. Incio para [depuración](https://nodejs.org/en/docs/guides/debugging-getting-started/) con npm:

    `NODE_ENV=production npm run start:debug`
    
3. Operaciones:
      
      1. Subida de archivos:
        
        curl \
          -F "email=atorres@conabio.gob.mx" \
          -F "layer=aves" \
          -F "geometry=@anpnov17gw.json" \
          "http://geoportal.conabio.gob.mx/servicios/datos/dinamicos/subida"
        
        Los tres campos son obligatorios, y el método http es únicamente POST
        
      2. Confirmación de la solicitud:
      
         http://geoportal.conabio.gob.mx/servicios/datos/avisos/?email=usuario@conabio.gob.mx&layer=aves&reqId=ia-iSr2hm
         
      3. Descarga de los datos (csv), geometría usada para filtrar (*.kml) y notas de la descarga (*.txt), todos compactados en zip:
      
         http://geoportal.conabio.gob.mx/descargas/mapas/dinamicos/aves.2018.07.25.qCwfeJvJR.csv.zip
         
4. Comentarios:
    * Los servicios están disponibles para todas las capas vectoriales del acervo (~9,000)
    * En el directorio `./logs` pueden existir (el servicio los crea la primera vez que los ocupe) dos archivos:
          * `subprocess.err`: En caso que ocurra una excepción durante el procesamiento de la consulta aquí apareceran los detalles
          * `subprocess.out`: No importa en realidad, si existe solo contiene mensajes de depuración cuando la ejecución del script fue en modo "development"
    * Los parámetros de GDAL, que determinan como se manipula la capa del usuario, se pueden ver `./lib/util.js (L:11)`
    * La especificación (máximo tamaño de archivo, número de campos del POST, etc.) de lo que el usuario puede subir al servidor están en `./lib/util.js (L:14-19)`
    * Los detalles sobre el servicio Web en sí mismo (el puerto que usa, la cuenta de correo desde donde envia los correos, etc.) se pueden ver en `./production.config.json`

**PENDIENTES**

* En producción hacer una rotación de ./logs/subprocess.err y en todo caso respaldar
* El contenido del archivo que sube el usuario no se valida ni se examina antes de pasarlo a GDAL, este es un aspecto importante a desarrollar. Un lugar donde podría ir esta validación es en `./lib/util.js (L:76)`
