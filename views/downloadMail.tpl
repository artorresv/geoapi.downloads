<div style="width:680px !important;margin: 0 auto !important;display: block !important;">
  <table style="font-family:sans-serif;border:1px solid #c0c0c0;font-size: 0.9em;padding: 4px;width: 100%;">
    <tr>
      <td colspan="2" style="text-transform: uppercase;font-weight: bolder;background-color: #ccc;padding: 6px;">Datos del mensaje</td>
    </tr>
    <tr>
      <td>Asunto:</td>
      <td>{{{subject}}}</td>
    </tr>
    <tr>
      <td>Enlace {{linkRel}}:</td>
      <td><a href="{{{downloadLink}}}">{{{downloadLink}}}</a></td>
    </tr>
    <tr>
      <td>Expiración del enlace:</td>
      <td>{{expirationDate}}</td>
    </tr>
    <tr>
      <td colspan="2" style="text-transform: uppercase;font-weight: bolder;background-color: #ccc;padding: 6px;">Datos de referencia</td>
    </tr>
    <tr>
      <td>Identificador del mapa:</td>
      <td><a title="Acceso directo" href="http://geoportal.conabio.gob.mx/#!l={{layerID}}:1">{{layerID}}</a></td>
    </tr>
    <tr>
      <td style="width:150px;">Responsable de los datos:</td>
      <td>{{contact1}}</td>
    </tr>
    <tr>
      <td style="width:150px;">Responsable del servicio:</td>
      <td>{{contact2}}</td>
    </tr>
    <tr>
      <td>Fecha de consulta:</td>
      <td>{{queryDate}}</td>
    </tr>
    <tr>
      <td colspan="2" style="text-transform: uppercase;font-weight: bolder;background-color: #ccc;padding: 6px;">Datos del usuario</td>
    </tr>
    <tr>
      <td>Nombre:</td>
      <td>{{userName}}</td>
    </tr>
    <tr>
      <td>Correo:</td>
      <td>{{userMail}}</td>
    </tr>
  </table>
  <p style="font-size: 0.9em;font-family: sans-serif;"><strong>Observaciones:</strong></p>
  <ul style="font-size: 0.9em;font-family: sans-serif;">
    <li>Si tiene algún comentario respecto a los datos, este servicio o cualquier otro asunto relacionado responda directamente al presente correo.</li>
    {{{observation1}}}
  </ul>
  <div style="font-weight: bolder;padding: 6px;font-family: sans-serif;font-size: 0.9em;border-top: 1px solid #c0c0c0;text-align: center;">
    <a title="Geoportal" href="http://geoportal.conabio.gob.mx/">Geoportal del Sistema Nacional de Información sobre Biodiversidad</a> - <a title="CONABIO" href="http://www.conabio.gob.mx/">CONABIO</a>
  </div>
</div>