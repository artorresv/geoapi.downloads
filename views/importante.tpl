Este archivo fue descargado del {{mdCreator}} el {{locDate}} a las {{locTime}}.

Los datos están actualizados al {{locLastMod}}.

* Para descargarlos nuevamente:
  {{{dataLink}}}
  
* Para ver los datos en el geoportal:
  {{{webLink}}}
  
* Para ver información detallada sobre las fuentes, descripción de los campos, etc.:
  {{{htmlLink}}}

Nombre del recurso:
  {{layerid}}.{{shortLastMod}}.{{suuid}}.csv.zip
  
  Donde:
    {{padLayerid}}: {{mdTitle}}
    {{shortLastMod}}   : actualizados al {{locLastMod}}
    {{suuid}}    : identificador único de la descarga
    csv          : en formato de texto delimitado por comas
    zip          : compactados en formato zip
  
Los archivos incluidos en la descarga son:
      
{{layerid}}.csv:
  {{recordCount}} registros extraídos del mapa "{{mdTitle}}", {{mdTitleDesc}}

limites.kml:
  Delimitación geográfica utilizada para extraer los registros del mapa "{{mdTitle}}". En formato de archivo KML.

importante.txt:
  Este documento