Este archivo fue descargado del {{mdCreator}} el {{locDate}} a las {{locTime}}.

Los datos están actualizados al {{locLastMod}}.

* Para descargarlos nuevamente:
  {{dataLink}}
  
* Para ver los datos en el geoportal:
  {{webLink}}
  
* Para ver la fuente de los datos:
  {{htmlLink}}

Nombre del recurso:
  {{layerid}}.{{shortLastMod}}.{{suuid}}.csv.zip
  
  Donde:
    {{padLayerid}}: {{mdTitle}}
    {{shortLastMod}}   : actualizados al {{locLastMod}}
    {{suuid}}    : identificador único de la descarga
    csv          : en formato de texto delimitado por comas
    zip          : compactados en formato zip
  
Los archivos incluidos en la descarga son:
      
{{layerid}}.csv:
  {{recordCount}} registros del mapa "{{mdTitle}}", {{mdTitleDesc}}

{{layerid}}.pdf (metadato): 
  Información detallada sobre los datos descargados, entre otros, fuentes, forma sugerida de citar, descripción de los campos, cobertura geográfica, etc.
  
limites.kml:
  Delimitación geográfica utilizada para extraer los registros del mapa "{{mdTitle}}". En formato de archivo KML.
